# Atlassian Connect JIRA Development Environment

This is a set of Vagrant / Puppet provisioning scripts for provisioning a local Atlassian Connect development environment. To build an Atlassian Connect Add-ons, you have two choices:

1. You can build your add-on locally and then register it in an OnDemand instance to test it.
2. You can build your add-on locally and then register it inside a local version JIRA or Confluence.

These provisioning scripts will allow you to build your add-on using option #2 above.

## Usage

First, install [VirtualBox](https://www.virtualbox.org/) and [Vagrant](http://www.vagrantup.com/) and make sure you have [git](http://git-scm.com/) available.

Open your favorite terminal and add a "base" virtual machine:

    vagrant box add base http://files.vagrantup.com/precise32.box

Clone the atlassian-connect-jira-vagrant project by typing at your command line:

    git clone https://bitbucket.org/rmanalan/atlassian-connect-jira-vagrant.git
    cd atlassian-connect-jira-vagrant

Start up and provision automatically all dependencies in the vm:

    vagrant up

The last step will provision your local JIRA server and run JIRA. It will take anywhere from 3-5 minutes for your server to be provisioned depending on the speed of your computer. Once it's done, you can access JIRA at:

    http://localhost:2990/jira (admin/admin)

### Registering your Atlassian Connect Add-on

Once your JIRA server is up and running, you can register your add-on by posting to installer resource:

    curl -v -u admin -X POST -d url=http://machine_hostname/path/to/your/atlassian-plugin.xml http://localhost:2990/jira/rest/remotable-plugins/latest/installer

Notice the URL we're posting to the Atlassian Connect installer (http://machine_hostname/path/to/your/atlassian-plugin.xml). The `machine_hostname` used here refers to your local machine's hostname. If you're on OSX, this typically will be your `machine_name.local`. You'll need to use the same hostname in your `atlassian-plugin.xml`:

    <remote-plugin-container key="container" display-url="http://machine_name.local:{port}">

This hostname is important since it needs to be accessible from within the guest VM as well as through the browser on the host machine.

### Contribute

This is a proof of concept Atlassian Connect development environment. There are other ways to do this, but if this helps please contribute.