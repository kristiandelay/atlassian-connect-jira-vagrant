include apt

exec { 'apt-get-update':
  command => 'apt-get update',
  path => "/usr/bin/:/bin/",
  before => Apt::Ppa["ppa:webupd8team/java"],
}

package { ["curl",
           "git-core",
           "bash"]:
  ensure => present,
  require => Exec["apt-get-update"],
}

class { 'java7':
  require => Exec['apt-get-update']
}

class { 'sudo': }
sudo::conf { 'vagrant':
  priority => 10,
  content  => "vagrant ALL=(ALL) NOPASSWD: ALL\n",
}

exec {
  "install_ap3":
  command => "sudo su -c \'curl https://bitbucket.org/atlassian/ap3-sdk-bin/raw/master/ap3-setup.sh | sh\' vagrant",
  creates => "/home/vagrant/.ap3-sdk",
  cwd => "/home/vagrant",
  user => "vagrant",
  path    => "/usr/bin/:/bin/",
  require => [ Class['java7'],
               Package["curl"],
               Package["git-core"],
               Class["sudo"] ],
  logoutput => false,
}

exec {
  "start_jira_in_background":
  command => "ap3 start jira --jvmargs '-Datlassian.upm.on.demand=true' &",
  cwd => "/vagrant",
  user => "vagrant",
  path    => "/usr/bin/:/bin/:/home/vagrant/.ap3-sdk/bin/",
  require => Exec["install_ap3"],
  logoutput => false,
  onlyif => 'test `ps aux | grep -c [t]omcat` -eq 0'
}

notify { 'done':
  message => "
    ***************************************************************************************************

      Congratulations! Your Atlassian Connect Development Box has been provisioned. In a few more minutes,
      you'll be able to access JIRA at:

          http://localhost:2990/jira (user: admin, password: admin)

      It might take 2-5 minutes to start up JIRA depending on how fast your machine is. In order to
      install your Atlassian Connect add-on, you can issue the following command from your terminal:

          curl -v -u admin -X POST -d url={url_where_your_atlassian-plugin.xml_resides}
          > http://localhost:2990/jira/rest/remotable-plugins/latest/installer

      That command relies the availability of the `curl` command on your machine.

    ***************************************************************************************************",
}

Exec['start_jira_in_background'] -> Notify['done']
